<?php

/**
 * @file
 * Install, update and uninstall functions for the imageproperty_check module.
 */

/**
 * Implements hook_schema().
 */
function imageproperty_check_schema() {
  $schema = array();
  $schema['imageproperty_check'] = array(
    'description' => 'This contains details of all the images which exceeds the certain file size limit',
    'fields' => array(
      'image_id' => array(
        'description' => 'This is for Id of image',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'image_name' => array(
        'description' => 'This is for image name',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'image_size' => array(
        'description' => 'This is for size of image',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'image_path' => array(
        'description' => 'This is for image path',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'image_filename' => array(
        'type' => 'varchar',
        'description' => "name of the file with jpg extension",
        'length' => 255,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('image_id'),
  );
  $schema['imageproperty_check_aspect_ratio'] = array(
    'description' => 'This table contains all the images with incorrect aspect ratio',
    'fields' => array(
      'image_id' => array(
        'description' => 'This is for Id of image',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'fid' => array(
        'description' => 'This is for fid of file',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'usage_count' => array(
        'description' => 'This is for usage count of file',
        'type' => 'int',
      ),
      'image_name' => array(
        'description' => 'This is for image name',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'image_original_aspect_ratio' => array(
        'description' => 'This is for original aspect ratio of image',
        'type' => 'float',
        'not null' => TRUE,
      ),
      'image_aspect_ratio' => array(
        'description' => 'This stores the aspect ratio of the modified uploaded image',
        'type' => 'float',
        'not null' => TRUE,
      ),
      'image_diff' => array(
        'description' => 'This is for status of images',
        'type' => 'float',
      ),
      'image_style' => array(
        'description' => 'This is for image style',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'image_path' => array(
        'description' => 'This is for image path',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('image_id'),
  );
  return $schema;
}
/**
 * Implements hook_uninstall().
 */
function imageproperty_check_uninstall() {
  drupal_uninstall_schema('imageproperty_check');
  drupal_uninstall_schema('imageproperty_check_aspect_ratio');
  $variables = db_select('variable', 'v')
    ->fields('v', array('name'))
    ->condition('name', 'imageproperty_check_type' . "%", 'LIKE')
    ->execute()
    ->fetchCol();
  foreach ($variables as $k) {
    variable_del($k);
  }
  variable_del('imageproperty_check_pager');
}

/**
 * Implements hook_requirements().
 */
function imageproperty_check_requirements($phase) {
  $description = '';
  $requirements = array();
  $t = get_t();
  $description .= ' ' . $t('There are certain images which degrade the performance of your website.You can view those images which exceed the specified size limit from<a href="@warning"> here</a>.', array('@warning' => url('admin/reports/image_preset_errors')));

  if ($phase == 'runtime') {
    $query = db_select('imageproperty_check', 'ip')
    ->fields('ip', array('image_id', 'image_name', 'image_size', 'image_path'))
    ->execute()
    ->fetchAssoc();
    if ($query) {
      $requirements['PRESET SIZES'] = array(
        'title' => $t('Image presets'),
        'description' => $description,
        'severity' => REQUIREMENT_WARNING,
        'weight' => -1000,
      );
    }
  }
  return $requirements;
}
